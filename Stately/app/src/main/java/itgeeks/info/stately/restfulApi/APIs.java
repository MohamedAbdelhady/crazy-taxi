package itgeeks.info.stately.restfulApi;

import com.google.gson.JsonObject;
import itgeeks.info.stately.restfulApi.RequestBody.RequestMainBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface APIs {

  @GET("version.json")
  Call<JsonObject> request();

}