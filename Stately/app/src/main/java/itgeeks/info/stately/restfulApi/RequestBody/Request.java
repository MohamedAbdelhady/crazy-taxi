package itgeeks.info.stately.restfulApi.RequestBody;

public class Request {

    private String email;

    private String password;

    private String name;

    private int country_id;

    private String membership;

    private String provider;

    private String provider_id;

    private String provider_name;

    private String provider_email;

    private String provider_image;

    public Request(String email, String password) { // login Request
        this.email = email;
        this.password = password;
    }

    public Request(String name, String email, int country_id, String pass) { // register Request
        this.name = name;
        this.email = email;
        this.membership = "basic";
        this.country_id = country_id;
        this.password = pass;
    }

    public Request(String provider, String provider_id, String provider_name, String provider_email, String provider_image, int country_id) { // Login and Register SocialMedia Request
        this.provider = provider;
        this.provider_id = provider_id;
        this.provider_name = provider_name;
        this.provider_email = provider_email;
        this.provider_image = provider_image;
        this.country_id = country_id;
    }

}