package itgeeks.info.stately.restfulApi;

import com.google.gson.JsonObject;

public interface HandleResponse {

    void handleTrueResponse(JsonObject mainObject);

    void handleFalseResponse(JsonObject errorObject);

    void handleEmptyResponse();

    void handleConnectionErrors(String errorMessage);

}
