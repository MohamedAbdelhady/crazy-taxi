package itgeeks.info.stately.restfulApi;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import java.io.IOException;

public class RetrofitClient {

    private static RetrofitClient mInstance;
    private Retrofit retrofit;

    private Call<JsonObject> call;

    private RetrofitClient() {
        this.retrofit = new Retrofit.Builder()
               // .client(new OkHttpClient.Builder().connectTimeout(20, TimeUnit.SECONDS).readTimeout(20, TimeUnit.SECONDS).writeTimeout(20, TimeUnit.SECONDS).build()) // time out
                .baseUrl("http://www.itgeeks.info/stately/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public static synchronized RetrofitClient getInstance() {
        // on creation
        if (mInstance == null ) {
            mInstance = new RetrofitClient();
        }
        return mInstance;
    }

    private String selectBaseUrl() {
        String BASE_URL = "";

//        switch (SharedPrefManager.getInstance(context).getSavedLang()) {
//            case "en":
//                BASE_URL = "http://elgawla.net/dev/public/api/v1/en/";
//                break;
//            case "ar":
//                BASE_URL = "http://elgawla.net/dev/public/api/v1/ar/";
//                break;
//            default:
//                BASE_URL = "http://elgawla.net/dev/public/api/v1/en/";
//                break;
//        }

        return BASE_URL;
    }

    public void executeConnectionToServer(Context context, HandleResponse HandleResponse) {
        call = getInstance().getAPI().request();
        call.enqueue(createWebserviceCallback(HandleResponse, context));
    }

    private APIs getAPI() {
        return retrofit.create(APIs.class);
    }

    private Callback<JsonObject> createWebserviceCallback(final HandleResponse HandleResponse, final Context context) {
        return new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.code() == 200) { // code == 200
                    try {
                        JsonObject mainObj = response.body().getAsJsonObject();

                        // dynamic with each call
                        HandleResponse.handleTrueResponse(mainObj);

                    } catch (NullPointerException e) { // errors of response body 'maybe response body has changed';
                        Log.e("onResponse: ", e.getMessage());
                        Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
                    } catch (UnsupportedOperationException e) {
                        Log.e("onResponse: ", e.getMessage());
                    }
                } else { // code != 200
                    try {
                        Toast.makeText(context, response.errorBody().string(), Toast.LENGTH_SHORT).show();
                        Log.d("error:", response.errorBody().string());
//                        // TODO: check codes instead of strings
//                        if (serverError.contains("not logged in") || serverError.contains("api token")) {
//                            context.startActivity(new Intent(context, ChooseLoginActivity.class)
//                                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
//
//                            SharedPrefManager.getInstance(context).clearUser();
//                        }
//
//                        // notify user
//                        Toast.makeText(context, serverError, Toast.LENGTH_SHORT).show();
//
//                        Log.d("!successful: ", serverError);
//                        // dynamic with each call
//                        HandleResponse.handleFalseResponse(errorObj);

                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JsonSyntaxException e) {
                        e.printStackTrace();
                    } catch (RuntimeException e) {
                        e.printStackTrace();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                // dynamic with each call
                HandleResponse.handleEmptyResponse();
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) { // connection errors
                Log.d("onFailure: ", t.getMessage());
                // dynamic with each call
                // HandleResponse.handleConnectionErrors(context.getString(R.string.no_connection));
                 HandleResponse.handleConnectionErrors(t.getMessage());
            }
        };
    }

    public void cancelCall() {
        if (call != null) {
            if (!call.isCanceled()) {
                call.cancel();
            }
        }
    }
}