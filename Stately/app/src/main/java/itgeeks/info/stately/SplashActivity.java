package itgeeks.info.stately;

import android.content.Intent;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.os.Handler;
import com.google.gson.JsonObject;
import itgeeks.info.stately.restfulApi.HandleResponse;
import itgeeks.info.stately.restfulApi.RetrofitClient;

public class SplashActivity extends AppCompatActivity {
    ImageView imgSplash;
    public String status;
    TextView commingSoon;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        initViews();

        RetrofitClient.getInstance().executeConnectionToServer(this, new HandleResponse() {
            @Override
            public void handleTrueResponse(JsonObject mainObject) {
                final String base_url = mainObject.get("base_url").getAsString();
                final String app_name = mainObject.get("app_name").getAsString();
                 status = mainObject.get("status").getAsString();

                if (status.equals("true")) {
                    imgSplash.setBackground(getDrawable(R.drawable.splash));
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Intent i = new Intent(SplashActivity.this, WebPageActivity.class);
                            commingSoon.setText("");
                            i.putExtra("base_url", base_url);
                            i.putExtra("app_name", app_name);
                            startActivity(i);
                            finish();
                        }
                    }, 1000);
                } else if (status.equals("false")){
                    imgSplash.setBackground(getDrawable(R.drawable.splash));
                    commingSoon.setText(getString(R.string.comming_soon));
                }

            }

            @Override
            public void handleFalseResponse(JsonObject errorObject) {

            }

            @Override
            public void handleEmptyResponse() {

            }

            @Override
            public void handleConnectionErrors(String errorMessage) {

            }
        });

    }

    private void initViews() {
        imgSplash = findViewById(R.id.image_splash);
        commingSoon = findViewById(R.id.comming_soon);
    }

    @Override
    public void onBackPressed() {
        if (status.equals("false")) {
            super.onBackPressed();
        }
    }
}
