package itgeeks.info.crazytaxi.views;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.view.*;
import android.widget.*;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.core.view.GravityCompat;
import androidx.appcompat.app.ActionBarDrawerToggle;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.navigation.NavigationView;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import io.paperdb.Paper;
import itgeeks.info.crazytaxi.General.LocaleHelper;
import itgeeks.info.crazytaxi.General.SharedPrefranceManager;
import itgeeks.info.crazytaxi.R;
import itgeeks.info.crazytaxi.views.Maps.RentalConfirmationActivity;
import itgeeks.info.crazytaxi.views.Account.ProfileActivity;
import itgeeks.info.crazytaxi.views.Menu.*;

import java.util.Locale;


public class HomeActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener, OnMapReadyCallback {

    public static Context langContext;
    public static Resources langResources;

    //toolbar
    private Toolbar toolbar;
    private EditText etDrop, etPickUp;

    // choose a car
    private TextView tcChooseAuto,tcChooseShare,tcChooseMicro,tcChooseOutStation,tvChooseTime1,tvChooseTime2,tvChooseTime3,tvChooseTime4;

    // sheet choose car
    private ImageView chooseCarAuto, chooseCarShare, chooseCarMicro, chooseCarMini;
    private TextView btnRideNow, btnRideLater;
    private static String choosedCar = "Auto";

    //    sheet Ride Now
    private TextView tvRideNowCarName, btnConfirmBooking;

    // Alert Dialog
    private TextView btnAlertOK;

    //map
    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        initViews();

        initChooseCar();

        // init language
        initLanguage();

        setSupportActionBar(toolbar);
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);

        final Menu menu = navigationView.getMenu();
        menu.add(0, R.id.nav_profile, 0, langResources.getString(R.string.my_profile)).setIcon(R.drawable.ic_person_black_24dp);
        menu.add(0, R.id.my_rides, 0, langResources.getString(R.string.my_rides)).setIcon(R.drawable.ic_directions_car_black_24dp);
        menu.add(0, R.id.your_bill, 0, langResources.getString(R.string.your_bill)).setIcon(R.drawable.ic_content_paste_black_24dp);
        menu.add(0, R.id.rate_card, 0, langResources.getString(R.string.rate_card)).setIcon(R.drawable.ic_content_paste_black_24dp);
        menu.add(0, R.id.learn_more, 0, langResources.getString(R.string.learn_more)).setIcon(R.drawable.ic_content_paste_black_24dp);
        menu.add(0, R.id.faqs, 0, langResources.getString(R.string.faqs)).setIcon(R.drawable.ic_content_paste_black_24dp);
        menu.add(0, R.id.support, 0, langResources.getString(R.string.support)).setIcon(R.drawable.ic_content_paste_black_24dp);

        final SubMenu subMenu = menu.addSubMenu(langResources.getString(R.string.langouage));
        subMenu.add(0, R.id.lang_ar, 0, langResources.getString(R.string.arabic));
        subMenu.add(0, R.id.lang_en, 0, langResources.getString(R.string.english));

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);

    }

    private void initViews() {
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        toolbar = findViewById(R.id.toolbar);

        etDrop = findViewById(R.id.tv_your_bill_hint3);
        etPickUp = findViewById(R.id.tv_your_bill_hint2);

        tvChooseTime1 = findViewById(R.id.tv_choose_car_time1);
        tvChooseTime2 = findViewById(R.id.tv_choose_car_time2);
        tvChooseTime3 = findViewById(R.id.tv_choose_car_time3);
        tvChooseTime4 = findViewById(R.id.tv_choose_car_time4);
        tcChooseAuto = findViewById(R.id.tv_choose_auto);
        tcChooseShare = findViewById(R.id.tv_choose_share);
        tcChooseMicro = findViewById(R.id.tv_choose_micro);
        tcChooseOutStation = findViewById(R.id.tv_choose_out_station);

        // set Actions
        setOnClickActions();
    }

    public void initLanguage() {

        // init Lang
        Paper.init(this);

        // Change Screen Dirction on API 27
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            if (Locale.getDefault().getLanguage().trim().equals("en")) {
//                getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
//            } else {
//                getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
//            }
//        }

        updateView(Paper.book().read("language").toString());
    }

    private void updateView(String lang) {
        langContext = LocaleHelper.setLocale(this, lang);
        langResources = langContext.getResources();
        toolbar.setSubtitle(langContext.getString(R.string.book_your_ride));

        etDrop.setHint(langResources.getString(R.string.drop));
        etPickUp.setHint(langResources.getString(R.string.pickup));
        tvChooseTime1.setText(langResources.getString(R.string.mins_10));
        tvChooseTime2.setText(langResources.getString(R.string.mins_3));
        tvChooseTime3.setText(langResources.getString(R.string.mins_5));
        tvChooseTime4.setText(langResources.getString(R.string.mins_20));
        tcChooseAuto.setText(langResources.getString(R.string.auto));
        tcChooseShare.setText(langResources.getString(R.string.share));
        tcChooseMicro.setText(langResources.getString(R.string.micro));
        tcChooseOutStation.setText(langResources.getString(R.string.out_station));
        btnRideNow.setText(langResources.getString(R.string.ride_now));
        btnRideLater.setText(langResources.getString(R.string.ride_later));

    }


    private void setOnClickActions() {
        etDrop.setOnClickListener(this);
        etPickUp.setOnClickListener(this);

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        // Add a marker in Sydney, Australia, and move the camera.
        LatLng sydney = new LatLng(30.0771, 31.2859);
        mMap.addMarker(new MarkerOptions().position(sydney).title(choosedCar).icon(BitmapDescriptorFactory.fromResource(R.drawable.car)));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney, 15));
        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                BottomSheetDialog bottomSheetDialogCallDriver = new BottomSheetDialog(HomeActivity.this, R.style.BottomSheetDialogTheme);
                View viewItemRideNow = LayoutInflater.from(HomeActivity.this).inflate(R.layout.item_call_driver, null);
                bottomSheetDialogCallDriver.setContentView(viewItemRideNow);
                bottomSheetDialogCallDriver.create();
                bottomSheetDialogCallDriver.show();

                TextView tvCallDriverHint1 = viewItemRideNow.findViewById(R.id.tv_call_driver_hint1);
                TextView tvCallDriverHint2 = viewItemRideNow.findViewById(R.id.tv_call_driver_hint2);
                TextView tvCallDriverHint3 = viewItemRideNow.findViewById(R.id.tv_call_driver_hint3);
                TextView tvCallDriverHint4 = viewItemRideNow.findViewById(R.id.tv_call_driver_hint4);
                TextView tvCallDriverHint5 = viewItemRideNow.findViewById(R.id.tv_call_driver_hint5);
                TextView tvCallDriverHint6 = viewItemRideNow.findViewById(R.id.tv_call_driver_hint6);
                tvCallDriverHint1.setText(langResources.getString(R.string.silver_sunny));
                tvCallDriverHint2.setText(langResources.getString(R.string.shankardass));
                tvCallDriverHint3.setText(langResources.getString(R.string.rating));
                tvCallDriverHint4.setText(langResources.getString(R.string.call_driver));
                tvCallDriverHint5.setText(langResources.getString(R.string.cancel));
                tvCallDriverHint6.setText(langResources.getString(R.string.estimated_cash_to_be_paid));
                tvCallDriverHint4.setOnClickListener(HomeActivity.this);

                // inVisable Choose car card
                findViewById(R.id.card_item_choose_car).setVisibility(View.GONE);
                bottomSheetDialogCallDriver.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        // Visable Choose car card if dismiss
                        findViewById(R.id.card_item_choose_car).setVisibility(View.VISIBLE);
                    }
                });
                return true;
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void initChooseCar() {
//        init Views
        chooseCarAuto = findViewById(R.id.choose_car_auto);
        chooseCarShare = findViewById(R.id.choose_car_share);
        chooseCarMicro = findViewById(R.id.choose_car_micro);
        chooseCarMini = findViewById(R.id.choose_car_mini);
        btnRideNow = findViewById(R.id.btn_ride_now);
        btnRideLater = findViewById(R.id.btn_alert_ride_later);
//        on Click to Choose car
        findViewById(R.id.choose_car_auto_div).setOnClickListener(this);
        findViewById(R.id.choose_car_share_div).setOnClickListener(this);
        findViewById(R.id.choose_car_micro_div).setOnClickListener(this);
        findViewById(R.id.choose_car_mini_div).setOnClickListener(this);

        btnRideNow.setOnClickListener(this);
        btnRideLater.setOnClickListener(this);
//        handle car
        switch (choosedCar) {
            case "Auto":
                effectChoosedAutoCar();
                break;
            case "Share":
                effectChoosedShareCar();
                break;
            case "Micro":
                effectChoosedMicroCar();
                break;
            case "Mini":
                effectChoosedMiniCar();
                break;
            case "Out station":
                effectChoosedMiniCar();
                break;
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_profile) {
            startActivity(new Intent(this, ProfileActivity.class));
        } else if (id == R.id.my_rides) {
            startActivity(new Intent(this, MyRidesActivity.class));
        } else if (id == R.id.your_bill) {
            startActivity(new Intent(this, YourBillActivity.class));
        } else if (id == R.id.rate_card) {
            startActivity(new Intent(this, RateCardActivity.class));
        } else if (id == R.id.learn_more) {
            startActivity(new Intent(this, LearnMoreActivity.class));
        } else if (id == R.id.faqs) {
            startActivity(new Intent(this, FaqsActivity.class));
        } else if (id == R.id.support) {
            startActivity(new Intent(this, SupportActivity.class));
        } else if (id == R.id.lang_ar) {
            SharedPrefranceManager.getInastance(HomeActivity.this).saveLang("ar");
            Paper.book().write("language", "ar");
            updateView(Paper.book().read("language").toString());
            recreate();
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            Toast.makeText(HomeActivity.this, "تم إختيار اللغة العربية", Toast.LENGTH_SHORT).show();
        } else if (id == R.id.lang_en) {
            SharedPrefranceManager.getInastance(HomeActivity.this).saveLang("en");
            Paper.book().write("language", "en");
            updateView(Paper.book().read("language").toString());
            // ((MainActivity)getContext()).initLanguage();
            recreate();
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            Toast.makeText(HomeActivity.this, "Selected English", Toast.LENGTH_SHORT).show();
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return false;
    }

    //    Open Fragement
    public void displayFragment(Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.content_home, fragment);
        fragmentTransaction.commit();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.choose_car_auto_div:
                choosedCar = "Auto";
                effectChoosedAutoCar(); // effect on click to choose auto car
                break;
            case R.id.choose_car_share_div:
                choosedCar = "Share";
                effectChoosedShareCar(); // effect on click to choose share car
                break;
            case R.id.choose_car_micro_div:
                choosedCar = "Micro";
                effectChoosedMicroCar(); // effect on click to choose micro car
                break;
            case R.id.choose_car_mini_div:
                choosedCar = langResources.getString(R.string.out_station);
                effectChoosedMiniCar(); // effect on click to choose mini car
                break;
            case R.id.btn_ride_now:
                if (choosedCar.trim().equals(langResources.getString(R.string.out_station))) {
                    startActivity(new Intent(this, OutstationRideActivity.class));
                } else {
                    BottomSheetDialog bottomSheetDialogRideNow = new BottomSheetDialog(this, R.style.BottomSheetDialogTheme);
                    View viewItemRideNow = LayoutInflater.from(this).inflate(R.layout.item_ride_now, null);
                    bottomSheetDialogRideNow.setContentView(viewItemRideNow);
                    bottomSheetDialogRideNow.create();
                    bottomSheetDialogRideNow.show();

                    //init Views
                    tvRideNowCarName = viewItemRideNow.findViewById(R.id.tv_ride_now_car_name);
                    btnConfirmBooking = viewItemRideNow.findViewById(R.id.btn_confirm_booking);
                    Spinner spinner = viewItemRideNow.findViewById(R.id.spinner_payment_ride_now);
                    TextView tvPersonalRideNow = viewItemRideNow.findViewById(R.id.tv_personal_ride_now);
                    EditText etCouponRideNow = viewItemRideNow.findViewById(R.id.et_coupon_ride_now);

                    String[] testArray = langResources.getStringArray(R.array.arr_payments);
                    ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                            this, android.R.layout.simple_dropdown_item_1line, testArray );
                    spinner.setAdapter(spinnerArrayAdapter);

                    // set Actions
                    tvPersonalRideNow.setText(langResources.getString(R.string.personal));
                    etCouponRideNow.setHint(langResources.getString(R.string.apply_coupen));
                    btnConfirmBooking.setText(langResources.getString(R.string.confirm_booking));
                    switch (choosedCar) {
                        case "Auto":
                            tvRideNowCarName.setText(langResources.getString(R.string.auto).toUpperCase());
                            toolbar.setSubtitle(langResources.getString(R.string.auto).toUpperCase());
                            break;
                        case "Share":
                            tvRideNowCarName.setText(langResources.getString(R.string.share).toUpperCase());
                            toolbar.setSubtitle(langResources.getString(R.string.share).toUpperCase());
                            break;
                        case "Micro":
                            tvRideNowCarName.setText(langResources.getString(R.string.micro).toUpperCase());
                            toolbar.setSubtitle(langResources.getString(R.string.micro).toUpperCase());
                            break;
                        case "Mini":
                            tvRideNowCarName.setText(langResources.getString(R.string.mini).toUpperCase());
                            toolbar.setSubtitle(langResources.getString(R.string.mini).toUpperCase());
                            break;
                        case "Out station":
                            tvRideNowCarName.setText(langResources.getString(R.string.out_station).toUpperCase());
                            toolbar.setSubtitle(langResources.getString(R.string.out_station).toUpperCase());
                            break;
                    }

                    btnConfirmBooking.setOnClickListener(this);

                    // on closed the sheet
                    bottomSheetDialogRideNow.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            toolbar.setSubtitle(langResources.getString(R.string.book_your_ride));
                        }
                    });
                }
                break;
            case R.id.btn_alert_ride_later:
                if (choosedCar.trim().equals(getString(R.string.out_station))) {
                    startActivity(new Intent(this, OutstationRideActivity.class));
                } else {
                    AlertDialog.Builder alertDialogRideLater = new AlertDialog.Builder(this);
                    View viewItemAlertRideLater = LayoutInflater.from(this).inflate(R.layout.dialog_ride_later, null);
                    alertDialogRideLater.setView(viewItemAlertRideLater);
                    alertDialogRideLater.create();
                    alertDialogRideLater.show();

                    //initViews
                    TextView tvAdvanveBooking = viewItemAlertRideLater.findViewById(R.id.tv_advance_booking);
                    TextView tvSelectData = viewItemAlertRideLater.findViewById(R.id.tv_select_data);
                    TextView tvSelectTime= viewItemAlertRideLater.findViewById(R.id.tv_select_time);
                    btnAlertOK = viewItemAlertRideLater.findViewById(R.id.btn_alert_OK);

                    // set Actions
                    tvAdvanveBooking.setText(langResources.getString(R.string.advance_booking));
                    tvSelectData.setText(langResources.getString(R.string.select_date));
                    tvSelectTime.setText(langResources.getString(R.string.select_time));
                    btnAlertOK.setText(langResources.getString(R.string.ok));
                    btnAlertOK.setOnClickListener(this);
                }
                break;
            case R.id.btn_alert_OK:
                BottomSheetDialog bottomSheetDialogRideLater = new BottomSheetDialog(this, R.style.BottomSheetDialogTheme);
                View viewItemRideLater = LayoutInflater.from(this).inflate(R.layout.item_ride_later, null);
                bottomSheetDialogRideLater.setContentView(viewItemRideLater);
                bottomSheetDialogRideLater.create();
                bottomSheetDialogRideLater.show();

                // init Views
                Spinner spinner = viewItemRideLater.findViewById(R.id.spinner_payment_ride_later);
                tvRideNowCarName = viewItemRideLater.findViewById(R.id.tv_ride_now_car_name);
                btnConfirmBooking = viewItemRideLater.findViewById(R.id.btn_confirm_booking);
                TextView tvPickupTimeRideLater = viewItemRideLater.findViewById(R.id.tv_pickup_time_ride_later);
                TextView tvPersonalRideLater = viewItemRideLater.findViewById(R.id.tv_personal_ride_later);
                EditText etApplyCouponRideLater = viewItemRideLater.findViewById(R.id.et_apply_coupon_ride_later);

                String[] testArray = langResources.getStringArray(R.array.arr_payments);
                ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                        this, android.R.layout.simple_dropdown_item_1line, testArray );
                spinner.setAdapter(spinnerArrayAdapter);


                // set Actions
                btnConfirmBooking.setOnClickListener(this);

                tvPickupTimeRideLater.setText(langResources.getString(R.string.pickup_time));
                tvPersonalRideLater.setText(langResources.getString(R.string.personal));
                etApplyCouponRideLater.setHint(langResources.getString(R.string.apply_coupen));
                btnConfirmBooking.setText(langResources.getString(R.string.confirm_booking));
                switch (choosedCar) {
                    case "Auto":
                        tvRideNowCarName.setText(langResources.getString(R.string.auto).toUpperCase());
                        toolbar.setSubtitle(langResources.getString(R.string.auto).toUpperCase());
                        break;
                    case "Share":
                        tvRideNowCarName.setText(langResources.getString(R.string.share).toUpperCase());
                        toolbar.setSubtitle(langResources.getString(R.string.share).toUpperCase());
                        break;
                    case "Micro":
                        tvRideNowCarName.setText(langResources.getString(R.string.micro).toUpperCase());
                        toolbar.setSubtitle(langResources.getString(R.string.micro).toUpperCase());
                        break;
                    case "Mini":
                        tvRideNowCarName.setText(langResources.getString(R.string.mini).toUpperCase());
                        toolbar.setSubtitle(langResources.getString(R.string.mini).toUpperCase());
                        break;
                    case "Out station":
                        tvRideNowCarName.setText(langResources.getString(R.string.out_station).toUpperCase());
                        toolbar.setSubtitle(langResources.getString(R.string.out_station).toUpperCase());
                        break;
                }

                // on closed the sheet
                bottomSheetDialogRideLater.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        toolbar.setSubtitle(langResources.getString(R.string.book_your_ride));
                    }
                });
                break;
            case R.id.tv_call_driver_hint4:
                BottomSheetDialog bottomSheetDialogmyRideScheduled = new BottomSheetDialog(this, R.style.BottomSheetDialogTheme);
                View viewItemRideScheduled = LayoutInflater.from(this).inflate(R.layout.item_my_ride_scheduled, null);
                bottomSheetDialogmyRideScheduled.setContentView(viewItemRideScheduled);
                bottomSheetDialogmyRideScheduled.create();
                bottomSheetDialogmyRideScheduled.show();

                TextView tvMyRideScheduledHint1,tvMyRideScheduledHint2,tvMyRideScheduledHint3,tvMyRideScheduledHint4,tvMyRideScheduledHint5,tvMyRideScheduledHint6,tvMyRideScheduledHint7,tvMyRideScheduledHint8;
                tvMyRideScheduledHint1 = viewItemRideScheduled.findViewById(R.id.tv_my_ride_scheduled_hint1);
                tvMyRideScheduledHint2 = viewItemRideScheduled.findViewById(R.id.tv_my_ride_scheduled_hint2);
                tvMyRideScheduledHint3 = viewItemRideScheduled.findViewById(R.id.tv_my_ride_scheduled_hint3);
                tvMyRideScheduledHint4 = viewItemRideScheduled.findViewById(R.id.tv_my_ride_scheduled_hint4);
                tvMyRideScheduledHint5 = viewItemRideScheduled.findViewById(R.id.tv_my_ride_scheduled_hint5);
                tvMyRideScheduledHint6 = viewItemRideScheduled.findViewById(R.id.tv_my_ride_scheduled_hint6);
                tvMyRideScheduledHint7 = viewItemRideScheduled.findViewById(R.id.tv_my_ride_scheduled_hint7);
                tvMyRideScheduledHint8 = viewItemRideScheduled.findViewById(R.id.tv_my_ride_scheduled_hint8);

                tvMyRideScheduledHint1.setText(langResources.getString(R.string.finished));
                tvMyRideScheduledHint2.setText(langResources.getString(R.string.my_ride_scheduled));
                tvMyRideScheduledHint3.setText(langResources.getString(R.string.tower_no));
                tvMyRideScheduledHint4.setText(langResources.getString(R.string.tower_no));
                tvMyRideScheduledHint5.setText(langResources.getString(R.string.use_coupen));
                tvMyRideScheduledHint6.setText(langResources.getString(R.string.tracking));
                tvMyRideScheduledHint7.setText(langResources.getString(R.string.cancel));
                tvMyRideScheduledHint8.setText(langResources.getString(R.string.support));
                break;
            case R.id.btn_confirm_booking:
                startActivity(new Intent(this, RentalConfirmationActivity.class));
                break;
            case R.id.tv_your_bill_hint3:
                break;
            case R.id.tv_your_bill_hint2:
                break;
        }
    }

    private void effectChoosedMiniCar() {
        chooseCarAuto.setBackground(getDrawable(R.color.white));
        chooseCarShare.setBackground(getDrawable(R.color.white));
        chooseCarMicro.setBackground(getDrawable(R.color.white));
        chooseCarMini.setBackground(getDrawable(R.drawable.cercle_yellow));
    }

    private void effectChoosedMicroCar() {
        chooseCarAuto.setBackground(getDrawable(R.color.white));
        chooseCarShare.setBackground(getDrawable(R.color.white));
        chooseCarMicro.setBackground(getDrawable(R.drawable.cercle_yellow));
        chooseCarMini.setBackground(getDrawable(R.color.white));
    }

    private void effectChoosedShareCar() {
        chooseCarAuto.setBackground(getDrawable(R.color.white));
        chooseCarShare.setBackground(getDrawable(R.drawable.cercle_yellow));
        chooseCarMicro.setBackground(getDrawable(R.color.white));
        chooseCarMini.setBackground(getDrawable(R.color.white));
    }

    private void effectChoosedAutoCar() {
        chooseCarAuto.setBackground(getDrawable(R.drawable.cercle_yellow));
        chooseCarShare.setBackground(getDrawable(R.color.white));
        chooseCarMicro.setBackground(getDrawable(R.color.white));
        chooseCarMini.setBackground(getDrawable(R.color.white));
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }
}
