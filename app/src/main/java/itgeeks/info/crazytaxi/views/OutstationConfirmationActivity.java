package itgeeks.info.crazytaxi.views;

import android.content.Intent;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import io.paperdb.Paper;
import itgeeks.info.crazytaxi.General.LocaleHelper;
import itgeeks.info.crazytaxi.R;

import static itgeeks.info.crazytaxi.views.HomeActivity.langContext;
import static itgeeks.info.crazytaxi.views.HomeActivity.langResources;

public class OutstationConfirmationActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView navName , btnConfirmOutstation;
    private TextView tvOutConfirmation_hint1,tvOutConfirmation_hint2,tvOutConfirmation_hint3,tvOutConfirmation_hint4,tvOutConfirmation_hint5,tvOutConfirmation_hint6,tvOutConfirmation_hint7,tvOutConfirmation_hint8,tvOutConfirmation_hint9,tvOutConfirmation_hint10;
    private EditText etCouponOutstationConfirmatiom;
    private Spinner spinnerConfirmation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_outstation_confirmation);

        initViews();

        // update language
        updateView(Paper.book().read("language").toString());

    }

    private void initViews() {

        // init Views
        navName = findViewById(R.id.nav_name);
        tvOutConfirmation_hint1 = findViewById(R.id.tv_outstation_confirmation_hint1);
        tvOutConfirmation_hint2 = findViewById(R.id.tv_outstation_confirmation_hint2);
        tvOutConfirmation_hint3 = findViewById(R.id.tv_outstation_confirmation_hint3);
        tvOutConfirmation_hint4 = findViewById(R.id.tv_outstation_confirmation_hint4);
        tvOutConfirmation_hint5 = findViewById(R.id.tv_outstation_confirmation_hint5);
        tvOutConfirmation_hint6 = findViewById(R.id.tv_outstation_confirmation_hint6);
        tvOutConfirmation_hint7 = findViewById(R.id.tv_outstation_confirmation_hint7);
        tvOutConfirmation_hint8 = findViewById(R.id.tv_outstation_confirmation_hint8);
        tvOutConfirmation_hint9 = findViewById(R.id.tv_outstation_confirmation_hint9);
        tvOutConfirmation_hint10 = findViewById(R.id.tv_outstation_confirmation_hint10);
        spinnerConfirmation = findViewById(R.id.spinner_confirmation);
        btnConfirmOutstation = findViewById(R.id.btn_confirm_outstation);
        etCouponOutstationConfirmatiom = findViewById(R.id.et_coupon_outstation_confirmatiom);

        //Actions
        btnConfirmOutstation.setOnClickListener(this);
        // set spinner data
        String[] testArray = langResources.getStringArray(R.array.arr_payments);
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                this, android.R.layout.simple_dropdown_item_1line, testArray );
        spinnerConfirmation.setAdapter(spinnerArrayAdapter);

        // back
        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void updateView(String lang) {
        langContext = LocaleHelper.setLocale(this, lang);
        langResources = langContext.getResources();
        navName.setText(langContext.getString(R.string.confirm_your_booking));

        tvOutConfirmation_hint1.setText(langResources.getString(R.string.mini));
        tvOutConfirmation_hint2.setText(langResources.getString(R.string.seater));
        tvOutConfirmation_hint3.setText(langResources.getString(R.string.your_location));
        tvOutConfirmation_hint4.setText(langResources.getString(R.string.tower_no));
        tvOutConfirmation_hint5.setText(langResources.getString(R.string.leave_on));
        tvOutConfirmation_hint6.setText(langResources.getString(R.string.out_station_confirmation_hint6));
//        tvOutConfirmation_hint7.setText(langResources.getString(R.string.your_location));
        tvOutConfirmation_hint8.setText(langResources.getString(R.string.estimated_fair));
        tvOutConfirmation_hint9.setText(langResources.getString(R.string.see_fair_details));
        tvOutConfirmation_hint10.setText(langResources.getString(R.string.personal));
        etCouponOutstationConfirmatiom.setHint(langResources.getString(R.string.apply_coupen));
        btnConfirmOutstation.setText(langResources.getString(R.string.accept_confirm));

    }

    public void openDialogEstimatedFairDetails(View view) {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        View view1 = LayoutInflater.from(this).inflate(R.layout.dialog_estimated_fair_details,null);
        alert.setView(view1);
        alert.create();
        alert.show();

        TextView dialogEstimatedHint1,dialogEstimatedHint2,dialogEstimatedHint3,dialogEstimatedHint4,dialogEstimatedHint5;
        //init dialog views
        dialogEstimatedHint1 = view1.findViewById(R.id.dialog_estimated_hint1);
        dialogEstimatedHint2 = view1.findViewById(R.id.dialog_estimated_hint2);
        dialogEstimatedHint3 = view1.findViewById(R.id.dialog_estimated_hint3);
        dialogEstimatedHint4 = view1.findViewById(R.id.dialog_estimated_hint4);
        dialogEstimatedHint5 = view1.findViewById(R.id.dialog_estimated_hint5);
        //update language
        dialogEstimatedHint1.setText(langResources.getString(R.string.estimated_fair_details));
        dialogEstimatedHint2.setText(langResources.getString(R.string.base_fair));
        dialogEstimatedHint3.setText(langResources.getString(R.string.driver_allowance));
        dialogEstimatedHint4.setText(langResources.getString(R.string.taxes));
        dialogEstimatedHint5.setText(langResources.getString(R.string.estimated_Fair));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_confirm_outstation:
                startActivity(new Intent(this,RideScheduledActivity.class));
                break;
        }
    }
}
