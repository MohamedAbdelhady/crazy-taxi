package itgeeks.info.crazytaxi.views;

import android.content.Intent;
import android.view.View;
import android.widget.RadioButton;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import io.paperdb.Paper;
import itgeeks.info.crazytaxi.General.LocaleHelper;
import itgeeks.info.crazytaxi.R;

import static itgeeks.info.crazytaxi.views.HomeActivity.langContext;
import static itgeeks.info.crazytaxi.views.HomeActivity.langResources;

public class OutstationRideActivity extends AppCompatActivity {

    private TextView navName;
    private TextView btnOpenOutstationConfirmation, tvOutstationHint1, tvOutstationHint2, tvOutstationHint3, tvOutstationHint4, tvOutstationHint5, tvOutstationHint6, tvOutstationHint7, tvOutstationHint8, tvOutstationHint9, tvOutstationHint10, tvOutstationHint11;
    private RadioButton radioOutstationHint1, radioOutstationHint2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_outstation_ride);

        initViews();

        // update language
        updateView(Paper.book().read("language").toString());

    }

    private void initViews() {
        // init Views
        navName = findViewById(R.id.nav_name);
        btnOpenOutstationConfirmation = findViewById(R.id.btn_open_outstation_confirmation);
        tvOutstationHint1 = findViewById(R.id.tv_outstation_hint1);
        tvOutstationHint2 = findViewById(R.id.tv_outstation_hint2);
        tvOutstationHint3 = findViewById(R.id.tv_outstation_hint3);
        tvOutstationHint4 = findViewById(R.id.tv_outstation_hint4);
        tvOutstationHint5 = findViewById(R.id.tv_outstation_hint5);
        tvOutstationHint6 = findViewById(R.id.tv_outstation_hint6);
        tvOutstationHint7 = findViewById(R.id.tv_outstation_hint7);
        tvOutstationHint8 = findViewById(R.id.tv_outstation_hint8);
        tvOutstationHint9 = findViewById(R.id.tv_outstation_hint9);
        tvOutstationHint10 = findViewById(R.id.tv_outstation_hint10);
        tvOutstationHint11 = findViewById(R.id.tv_outstation_hint11);

        radioOutstationHint1 = findViewById(R.id.radio_outstation_hint1);
        radioOutstationHint2 = findViewById(R.id.radio_outstation_hint2);

        // back
        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void updateView(String lang) {
        langContext = LocaleHelper.setLocale(this, lang);
        langResources = langContext.getResources();
        navName.setText(langContext.getString(R.string.book_your_outstation_ride));

        tvOutstationHint1.setText(langResources.getString(R.string.your_location));
        tvOutstationHint2.setText(langResources.getString(R.string.tower_no));
        tvOutstationHint3.setText(langResources.getString(R.string.select_date_time));
        tvOutstationHint4.setText(langResources.getString(R.string.leave_on));
        tvOutstationHint5.setText(langResources.getString(R.string.select_vehical_type));
        tvOutstationHint6.setText(langResources.getString(R.string.mini));
        tvOutstationHint7.setText(langResources.getString(R.string.out_station_hint7));
        tvOutstationHint8.setText(langResources.getString(R.string.out_station_hint8));
        tvOutstationHint9.setText(langResources.getString(R.string.micro));
        tvOutstationHint10.setText(langResources.getString(R.string.out_station_hint7));
        tvOutstationHint11.setText(langResources.getString(R.string.out_station_hint8));
        btnOpenOutstationConfirmation.setText(langResources.getString(R.string.ok));
        radioOutstationHint1.setText(langResources.getString(R.string.one_way));
        radioOutstationHint2.setText(langResources.getString(R.string.round_trip));

    }

    public void OpenOutstationConfirmation(View view) {
        startActivity(new Intent(this, OutstationConfirmationActivity.class));
    }
}
