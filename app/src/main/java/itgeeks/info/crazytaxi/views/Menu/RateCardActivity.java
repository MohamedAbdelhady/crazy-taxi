package itgeeks.info.crazytaxi.views.Menu;

import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import io.paperdb.Paper;
import itgeeks.info.crazytaxi.General.LocaleHelper;
import itgeeks.info.crazytaxi.R;

import java.lang.reflect.Array;

import static itgeeks.info.crazytaxi.views.HomeActivity.langContext;
import static itgeeks.info.crazytaxi.views.HomeActivity.langResources;

public class RateCardActivity extends AppCompatActivity {

    private TextView navName;
    private TextView tvRateCardHint1,tvRateCardHint2,tvRateCardHint3,tvRateCardHint4,tvRateCardHint5,tvRateCardHint6,tvRateCardHint7,tvRateCardHint8;
    private Spinner spinnerCity, spinnerCars;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rate_card);

        initViews();

        // Update Language
        updateView(Paper.book().read("language").toString());

    }

    private void initViews() {

        // initViews
        navName = findViewById(R.id.nav_name);
        spinnerCity = findViewById(R.id.spinner_city);
        spinnerCars = findViewById(R.id.spinner_cars);

        tvRateCardHint1 = findViewById(R.id.tv_rate_card_hint1);
        tvRateCardHint2 = findViewById(R.id.tv_rate_card_hint2);
        tvRateCardHint3 = findViewById(R.id.tv_rate_card_hint3);
        tvRateCardHint4 = findViewById(R.id.tv_rate_card_hint4);
        tvRateCardHint5 = findViewById(R.id.tv_rate_card_hint5);
        tvRateCardHint6 = findViewById(R.id.tv_rate_card_hint6);
        tvRateCardHint7 = findViewById(R.id.tv_rate_card_hint7);
        tvRateCardHint8 = findViewById(R.id.tv_rate_card_hint8);


        // init Spinner City
        String[] ArrayCity = langResources.getStringArray(R.array.arr_city);
        ArrayAdapter<String> spinnerCityArrayAdapter = new ArrayAdapter<String>(
                this, android.R.layout.simple_spinner_dropdown_item, ArrayCity);
        // init Spinner Car
        String[] ArrayCar = langResources.getStringArray(R.array.arr_cars);
        ArrayAdapter<String> spinnerCarArrayAdapter = new ArrayAdapter<String>(
                this, android.R.layout.simple_spinner_dropdown_item, ArrayCar);

        spinnerCity.setAdapter(spinnerCityArrayAdapter);
        spinnerCars.setAdapter(spinnerCarArrayAdapter);

        // setActions
        setActions();

        // back
        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void updateView(String lang) {
        langContext = LocaleHelper.setLocale(this, lang);
        langResources = langContext.getResources();

        navName.setText(langResources.getString(R.string.rate_card));

        tvRateCardHint1.setText(langResources.getString(R.string.select_city));
        tvRateCardHint2.setText(langResources.getString(R.string.select_category));
        tvRateCardHint3.setText(langResources.getString(R.string.regular));
        tvRateCardHint4.setText(langResources.getString(R.string.mini));
        tvRateCardHint5.setText(langResources.getString(R.string.seater));
        tvRateCardHint6.setText(langResources.getString(R.string.total_fair));
        tvRateCardHint7.setText(langResources.getString(R.string.extra_charges));
        tvRateCardHint8.setText(langResources.getString(R.string.cancellation_fee));

    }

    private void setActions() {

    }
}