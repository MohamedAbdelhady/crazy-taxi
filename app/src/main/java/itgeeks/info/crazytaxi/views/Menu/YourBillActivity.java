package itgeeks.info.crazytaxi.views.Menu;

import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import io.paperdb.Paper;
import itgeeks.info.crazytaxi.General.LocaleHelper;
import itgeeks.info.crazytaxi.R;

import static itgeeks.info.crazytaxi.views.HomeActivity.langContext;
import static itgeeks.info.crazytaxi.views.HomeActivity.langResources;

public class YourBillActivity extends AppCompatActivity {

    private TextView navName;
    private TextView tvYourBillHint1, tvYourBillHint2, tvYourBillHint3, tvYourBillHint4 , btnYourBillSubmit;
    private EditText etYourBillComment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_your_bill);
        initViews();

        // Update Language
        updateView(Paper.book().read("language").toString());

    }

    private void initViews() {
        // init Views
        navName = findViewById(R.id.nav_name);
        tvYourBillHint1 = findViewById(R.id.tv_your_bill_hint1);
        tvYourBillHint2 = findViewById(R.id.tv_your_bill_hint2);
        tvYourBillHint3 = findViewById(R.id.tv_your_bill_hint3);
        tvYourBillHint4 = findViewById(R.id.tv_your_bill_hint4);
        etYourBillComment = findViewById(R.id.et_your_bill_comment);
        btnYourBillSubmit = findViewById(R.id.btn_your_bill_submit);

        // setActions
        setActions();

        // back
        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void updateView(String lang) {
        langContext = LocaleHelper.setLocale(this, lang);
        langResources = langContext.getResources();

        navName.setText(langResources.getString(R.string.your_bill));
        tvYourBillHint1.setText(langResources.getString(R.string.discount_applied));
        tvYourBillHint2.setText(langResources.getString(R.string.tower_no));
        tvYourBillHint3.setText(langResources.getString(R.string.tower_no));
        tvYourBillHint4.setText(langResources.getString(R.string.How_was_the_ride));
        etYourBillComment.setHint(langResources.getString(R.string.comments));
        btnYourBillSubmit.setText(langResources.getString(R.string.submit));


    }

    private void setActions() {

    }
}