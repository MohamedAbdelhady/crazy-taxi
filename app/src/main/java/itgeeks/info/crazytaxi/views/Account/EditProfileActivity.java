package itgeeks.info.crazytaxi.views.Account;

import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import com.google.android.material.textfield.TextInputLayout;
import io.paperdb.Paper;
import itgeeks.info.crazytaxi.General.LocaleHelper;
import itgeeks.info.crazytaxi.R;

import static itgeeks.info.crazytaxi.views.HomeActivity.langContext;
import static itgeeks.info.crazytaxi.views.HomeActivity.langResources;

public class EditProfileActivity extends AppCompatActivity {

    private TextView navName;
    private TextInputLayout tvEditProfilePhone ,tvEditProfileName ,tvEditProfileEmail ,tvEditProfileAddress ,tvEditProfileCountry ,tvEditProfileState ,tvEditProfileCity ,tvEditProfileZipCode;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);

        initViews();

        // Update Language
        updateView(Paper.book().read("language").toString());
    }

    private void initViews() {

        navName = findViewById(R.id.nav_name);
        tvEditProfilePhone = findViewById(R.id.tv_edit_profile_phone);
        tvEditProfileName = findViewById(R.id.tv_edit_profile_name);
        tvEditProfileEmail = findViewById(R.id.tv_edit_profile_email);
        tvEditProfileAddress = findViewById(R.id.tv_edit_profile_address);
        tvEditProfileCountry = findViewById(R.id.tv_edit_profile_country);
        tvEditProfileState = findViewById(R.id.tv_edit_profile_state);
        tvEditProfileCity = findViewById(R.id.tv_edit_profile_city);
        tvEditProfileZipCode = findViewById(R.id.tv_edit_profile_zip_code);

        // back
        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void updateView(String lang) {
        langContext = LocaleHelper.setLocale(this, lang);
        langResources = langContext.getResources();

        navName.setText(langResources.getString(R.string.update_profile));
        tvEditProfilePhone.setHint(langResources.getString(R.string.mobile_number));
        tvEditProfileName.setHint(langResources.getString(R.string.name));
        tvEditProfileEmail.setHint(langResources.getString(R.string.email));
        tvEditProfileAddress.setHint(langResources.getString(R.string.address));
        tvEditProfileCountry.setHint(langResources.getString(R.string.country));
        tvEditProfileState.setHint(langResources.getString(R.string.state));
        tvEditProfileCity.setHint(langResources.getString(R.string.city));
        tvEditProfileZipCode.setHint(langResources.getString(R.string.zipcode));


    }
}