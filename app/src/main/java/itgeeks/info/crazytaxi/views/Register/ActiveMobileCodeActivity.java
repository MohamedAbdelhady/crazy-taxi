package itgeeks.info.crazytaxi.views.Register;

import android.content.Intent;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import io.paperdb.Paper;
import itgeeks.info.crazytaxi.General.LocaleHelper;
import itgeeks.info.crazytaxi.R;

import static itgeeks.info.crazytaxi.views.SplashActivity.langContext;
import static itgeeks.info.crazytaxi.views.SplashActivity.langResources;

public class ActiveMobileCodeActivity extends AppCompatActivity {

    EditText active_num1, active_num2, active_num3, active_num4;
    TextView navName, ActiveCodeHint1, ActiveCodeHint3,tvVerify;
    private LinearLayout btnActiveCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        setContentView(R.layout.activity_active_mobile_code);

        initViews();

        updateView(Paper.book().read("language").toString());

    }

    private void initViews() {

        navName = findViewById(R.id.nav_name);

        btnActiveCode = findViewById(R.id.btn_active_mobile_code);
        ActiveCodeHint1 = findViewById(R.id.Active_code_hint1);
        ActiveCodeHint3 = findViewById(R.id.Active_code_hint3);
        tvVerify = findViewById(R.id.tv_verify);

        active_num1 = findViewById(R.id.active_num1);
        active_num2 = findViewById(R.id.active_num2);
        active_num3 = findViewById(R.id.active_num3);
        active_num4 = findViewById(R.id.active_num4);

        //change input
        active_num1.addTextChangedListener(new GenericTextWatcher(active_num1));
        active_num2.addTextChangedListener(new GenericTextWatcher(active_num2));
        active_num3.addTextChangedListener(new GenericTextWatcher(active_num3));
        active_num4.addTextChangedListener(new GenericTextWatcher(active_num4));

        btnActiveCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ActiveMobileCodeActivity.this, CreateAcountActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP));
            }
        });

        // back
        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void updateView(String lang) {
        langContext = LocaleHelper.setLocale(this, lang);
        langResources = langContext.getResources();

        navName.setText(langResources.getString(R.string.Active_code));
        ActiveCodeHint1.setText(langResources.getString(R.string.active_code_hint));
        ActiveCodeHint3.setText(langResources.getString(R.string.active_code_hint2));
        tvVerify.setText(langResources.getString(R.string.verify));

    }

    // TextWatcher Active Code
    public class GenericTextWatcher implements TextWatcher {
        private View view;

        private GenericTextWatcher(View view) {
            this.view = view;
        }

        @Override
        public void afterTextChanged(Editable editable) {
            // TODO Auto-generated method stub
            String text = editable.toString();
            switch (view.getId()) {
                case R.id.active_num1:
                    if (text.length() == 1)
                        active_num2.requestFocus();
                    break;
                case R.id.active_num2:
                    if (text.length() == 1)
                        active_num3.requestFocus();
                    else if (text.length() == 0)
                        active_num1.requestFocus();
                    break;
                case R.id.active_num3:
                    if (text.length() == 1)
                        active_num4.requestFocus();
                    else if (text.length() == 0)
                        active_num2.requestFocus();
                    break;
                case R.id.active_num4:
                    if (text.length() == 0)
                        active_num3.requestFocus();
                    break;
            }
        }

        @Override
        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            // TODO Auto-generated method stub
        }

        @Override
        public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            // TODO Auto-generated method stub
        }
    }

}
