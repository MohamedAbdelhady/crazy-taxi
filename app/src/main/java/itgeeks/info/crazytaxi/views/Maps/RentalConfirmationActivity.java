
package itgeeks.info.crazytaxi.views.Maps;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import io.paperdb.Paper;
import itgeeks.info.crazytaxi.General.LocaleHelper;
import itgeeks.info.crazytaxi.R;

import static itgeeks.info.crazytaxi.views.HomeActivity.langContext;
import static itgeeks.info.crazytaxi.views.HomeActivity.langResources;

public class RentalConfirmationActivity extends AppCompatActivity {

    private TextView navName,tvdialogRentATitle;
    private TextView tvRentalHint1,tvRentalHint2,tvRentalHint3,tvRentalHint4,tvRentalHint5,tvRentalHint6,tvRentalHint7,tvRentalHint8,etRentalCoupon,btnConfirmBooking;
    private TextView tvRentalAlertHint1,tvRentalAlertHint2,tvRentalAlertHint3,tvRentalAlertHint4,tvRentalAlertHint5,tvRentalAlertHint6;
    private Spinner spinnerRental;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rental_confirmation);

        initViews();

        // Update Language
        updateView(Paper.book().read("language").toString());

    }

    private void initViews() {

        // init Views
        navName = findViewById(R.id.nav_name);
        tvRentalHint1 = findViewById(R.id.tv_rental_hint1);
        tvRentalHint2 = findViewById(R.id.tv_rental_hint2);
        tvRentalHint3 = findViewById(R.id.tv_rental_hint3);
        tvRentalHint4 = findViewById(R.id.tv_rental_hint4);
        tvRentalHint5 = findViewById(R.id.tv_rental_hint5);
        tvRentalHint6 = findViewById(R.id.tv_rental_hint6);
        tvRentalHint7 = findViewById(R.id.tv_rental_hint7);
        tvRentalHint8 = findViewById(R.id.tv_rental_hint8);
        etRentalCoupon = findViewById(R.id.et_rental_coupon);
        btnConfirmBooking = findViewById(R.id.btn_confirm_booking);
        spinnerRental = findViewById(R.id.spinner_rental);


        String[] testArray = langResources.getStringArray(R.array.arr_payments);
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                this, android.R.layout.simple_dropdown_item_1line, testArray );
        spinnerRental.setAdapter(spinnerArrayAdapter);

        // back
        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void updateView(String lang) {
        langContext = LocaleHelper.setLocale(this, lang);
        langResources = langContext.getResources();

        navName.setText(langResources.getString(R.string.book_your_ride));

        tvRentalHint1.setText(langResources.getString(R.string.pickup_location));
        tvRentalHint2.setText(langResources.getString(R.string.tower_no));
        tvRentalHint3.setText(langResources.getString(R.string.duration));
        tvRentalHint4.setText(langResources.getString(R.string.rental_hint4));
        tvRentalHint5.setText(langResources.getString(R.string.duration));
        tvRentalHint6.setText(langResources.getString(R.string.rental_hint6));
        tvRentalHint7.setText(langResources.getString(R.string.fair_details_rules));
        tvRentalHint8.setText(langResources.getString(R.string.personal));

        etRentalCoupon.setHint(langResources.getString(R.string.apply_coupen));
        btnConfirmBooking.setText(langResources.getString(R.string.accept_confirm));

    }

    public void btnOpenRules(View view) {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        View view1 = LayoutInflater.from(this).inflate(R.layout.dialog_rent_a,null);
        tvdialogRentATitle = view1.findViewById(R.id.dialog_rent_a_title);
        tvRentalAlertHint1 = view1.findViewById(R.id.tv_rental_alert_hint1);
        tvRentalAlertHint2 = view1.findViewById(R.id.tv_rental_alert_hint2);
        tvRentalAlertHint3 = view1.findViewById(R.id.tv_rental_alert_hint3);
        tvRentalAlertHint4 = view1.findViewById(R.id.tv_rental_alert_hint4);
        tvRentalAlertHint5 = view1.findViewById(R.id.tv_rental_alert_hint5);
        tvRentalAlertHint6 = view1.findViewById(R.id.tv_rental_alert_hint6);

        tvdialogRentATitle.setText(langResources.getString(R.string.rent_a_micro));
        tvRentalAlertHint1.setText(langResources.getString(R.string.base_fair));
        tvRentalAlertHint2.setText(langResources.getString(R.string.rental_alert_hint2));
        tvRentalAlertHint3.setText(langResources.getString(R.string.additional_km_fairs));
        tvRentalAlertHint4.setText(langResources.getString(R.string.rental_alert_hint4));
        tvRentalAlertHint5.setText(langResources.getString(R.string.additional_ride_time_fair));
        tvRentalAlertHint6.setText(langResources.getString(R.string.rental_alert_hint6));

        alert.setView(view1);
        alert.create();
        alert.show();
    }
}
