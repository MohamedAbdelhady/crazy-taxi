package itgeeks.info.crazytaxi.views.Register;

import android.content.Intent;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import io.paperdb.Paper;
import itgeeks.info.crazytaxi.General.LocaleHelper;
import itgeeks.info.crazytaxi.General.SharedPrefranceManager;
import itgeeks.info.crazytaxi.R;

import static itgeeks.info.crazytaxi.views.Register.RegisterActivity.*;
import static itgeeks.info.crazytaxi.views.SplashActivity.langContext;
import static itgeeks.info.crazytaxi.views.SplashActivity.langResources;


public class CreateAcountActivity extends AppCompatActivity implements View.OnClickListener {
    private TextView navName ,createAccountHint1 ,tvCreateAcountHint3,tvCreateAcountHint4,tvCreateAccountRegister,tvCreateAcountHint2_1,tvCreateAcountHint2_2;
    private EditText etFullName , etEmail;
    private LinearLayout btnCreateAcount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_acount);

        initViews();

        updateView(Paper.book().read("language").toString());

    }

    private void initViews() {
        // Nav Name
        navName = findViewById(R.id.nav_name);

        //initViews
        etFullName = findViewById(R.id.et_full_name);
        etEmail = findViewById(R.id.et_email);
        createAccountHint1 = findViewById(R.id.tv_create_account_hint1);
        tvCreateAcountHint2_1 = findViewById(R.id.tv_create_acount_hint2_1);
        tvCreateAcountHint2_2 = findViewById(R.id.tv_create_acount_hint2_2);
        tvCreateAcountHint3 = findViewById(R.id.tv_create_acount_hint3);
        tvCreateAcountHint4 = findViewById(R.id.tv_create_acount_hint4);
        tvCreateAccountRegister = findViewById(R.id.tv_create_account_register);
        btnCreateAcount = findViewById(R.id.btn_create_acount);

        // set Actions
        setActions();

        // back
        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void updateView(String lang) {
        langContext = LocaleHelper.setLocale(this, lang);
        langResources = langContext.getResources();

        navName.setText(langResources.getString(R.string.create_account));
        etFullName.setHint(langResources.getString(R.string.full_name));
        etEmail.setHint(langResources.getString(R.string.email_optional));
        createAccountHint1.setText(langResources.getString(R.string.create_account_hint1));
        tvCreateAcountHint2_1.setText(langResources.getString(R.string.got_refferal_code));
        tvCreateAcountHint2_2.setText(langResources.getString(R.string.apply_here));
        tvCreateAcountHint3.setText(langResources.getString(R.string.create_account_hint3));
        tvCreateAcountHint4.setText(langResources.getString(R.string.out_terms));
        tvCreateAccountRegister.setText(langResources.getString(R.string.register));

    }

    private void setActions() {
        btnCreateAcount.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case  R.id.btn_create_acount:
                startActivity(new Intent(this,CongratulationsActivity.class));
                break;
        }
    }
}
