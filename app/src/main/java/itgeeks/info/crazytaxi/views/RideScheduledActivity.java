package itgeeks.info.crazytaxi.views;

import android.content.Intent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.cardview.widget.CardView;
import io.paperdb.Paper;
import itgeeks.info.crazytaxi.General.LocaleHelper;
import itgeeks.info.crazytaxi.R;

import static itgeeks.info.crazytaxi.views.HomeActivity.langContext;
import static itgeeks.info.crazytaxi.views.HomeActivity.langResources;

public class RideScheduledActivity extends AppCompatActivity implements View.OnClickListener {

    TextView navName;
    TextView tvCongratulationsHint1, tvCongratulationsHint2, tvCongratulationsHint3;
    private LinearLayout btnRegisterCongratulations;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ride_scheduled);

        initViews();

        // update language
        updateView(Paper.book().read("language").toString());

    }

    private void initViews() {
        // init Views
        navName = findViewById(R.id.nav_name);
        tvCongratulationsHint1 = findViewById(R.id.tv_congratulations_hint1);
        tvCongratulationsHint2 = findViewById(R.id.tv_congratulations_hint2);
        tvCongratulationsHint3 = findViewById(R.id.tv_congratulations_hint3);
        btnRegisterCongratulations = findViewById(R.id.btn_scheduled_congratulations);

        // set Actions
        setActions();

    }

    private void updateView(String lang) {
        langContext = LocaleHelper.setLocale(this, lang);
        langResources = langContext.getResources();
        navName.setText(langContext.getString(R.string.ride_scheduled));

        tvCongratulationsHint1.setText(langResources.getString(R.string.ride_scheduled));
        tvCongratulationsHint2.setText(langResources.getString(R.string.Your_account_has_been_scheduled));
        tvCongratulationsHint3.setText(langResources.getString(R.string.got_it));

    }

    private void setActions() {
        btnRegisterCongratulations.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_scheduled_congratulations:
                startActivity(new Intent(this, HomeActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                break;
        }
    }
}
