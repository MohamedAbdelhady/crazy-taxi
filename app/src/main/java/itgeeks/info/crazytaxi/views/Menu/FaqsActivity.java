package itgeeks.info.crazytaxi.views.Menu;

import android.view.View;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import io.paperdb.Paper;
import itgeeks.info.crazytaxi.General.LocaleHelper;
import itgeeks.info.crazytaxi.R;

import static itgeeks.info.crazytaxi.views.HomeActivity.langContext;
import static itgeeks.info.crazytaxi.views.HomeActivity.langResources;

public class FaqsActivity extends AppCompatActivity {

    private TextView navName;
    private TextView tvFaqsHint1,tvFaqsHint2,tvFaqsHint3,tvFaqsHint4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faqs);

        initViews();

        // Update Language
        updateView(Paper.book().read("language").toString());

    }

    private void initViews() {
        // Nav Name
        navName = findViewById(R.id.nav_name);

        tvFaqsHint1 = findViewById(R.id.tv_faqs_hint1);
        tvFaqsHint2 = findViewById(R.id.tv_faqs_hint2);
        tvFaqsHint3 = findViewById(R.id.tv_faqs_hint3);
        tvFaqsHint4 = findViewById(R.id.tv_faqs_hint4);

        // setActions
        setActions();

        // back
        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void updateView(String lang) {
        langContext = LocaleHelper.setLocale(this, lang);
        langResources = langContext.getResources();

        navName.setText(langResources.getString(R.string.faqs));

        tvFaqsHint1.setText(langResources.getString(R.string.faqs_hint1));
        tvFaqsHint2.setText(langResources.getString(R.string.faqs_hint1));
        tvFaqsHint3.setText(langResources.getString(R.string.faqs_hint1));
        tvFaqsHint4.setText(langResources.getString(R.string.faqs_hint1));

    }

    private void setActions() {

    }
}