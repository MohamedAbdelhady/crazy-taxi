package itgeeks.info.crazytaxi.views.Account;

import android.content.Intent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import io.paperdb.Paper;
import itgeeks.info.crazytaxi.General.LocaleHelper;
import itgeeks.info.crazytaxi.R;

import static itgeeks.info.crazytaxi.views.HomeActivity.langContext;
import static itgeeks.info.crazytaxi.views.HomeActivity.langResources;

public class ProfileActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView imgEditProfile;
    private TextView navName;
    private TextView tvProfileHint1, tvProfileHint2, tvProfileHint3, tvProfileHint4, tvProfileHint5, tvProfileHint6, tvProfileHint7, tvProfileHint8, tvProfileHint9;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        initViews();

        // Update Language
        updateView(Paper.book().read("language").toString());

    }

    private void initViews() {
        // Nav Name
        navName = findViewById(R.id.nav_name);

        // initViews
        imgEditProfile = findViewById(R.id.profile_edit);
        tvProfileHint1 = findViewById(R.id.tv_profile_hint1);
        tvProfileHint2 = findViewById(R.id.tv_profile_hint2);
        tvProfileHint3 = findViewById(R.id.tv_profile_hint3);
        tvProfileHint4 = findViewById(R.id.tv_profile_hint4);
        tvProfileHint5 = findViewById(R.id.tv_profile_hint5);
        tvProfileHint6 = findViewById(R.id.tv_profile_hint6);
        tvProfileHint7 = findViewById(R.id.tv_profile_hint7);
        tvProfileHint8 = findViewById(R.id.tv_profile_hint8);
        tvProfileHint9 = findViewById(R.id.tv_profile_hint9);

        // setActions
        setActions();

        // back
        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void updateView(String lang) {
        langContext = LocaleHelper.setLocale(this, lang);
        langResources = langContext.getResources();

        navName.setText(langResources.getString(R.string.my_profile));
        tvProfileHint1.setText(langResources.getString(R.string.mobile_number));
        tvProfileHint2.setText(langResources.getString(R.string.name));
        tvProfileHint3.setText(langResources.getString(R.string.email));
        tvProfileHint4.setText(langResources.getString(R.string.address));
        tvProfileHint5.setText(langResources.getString(R.string.address));
        tvProfileHint6.setText(langResources.getString(R.string.country));
        tvProfileHint7.setText(langResources.getString(R.string.state));
        tvProfileHint8.setText(langResources.getString(R.string.city));
        tvProfileHint9.setText(langResources.getString(R.string.zipcode));

    }

    private void setActions() {
        imgEditProfile.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.profile_edit:
                startActivity(new Intent(this, EditProfileActivity.class));
                break;
        }
    }
}
