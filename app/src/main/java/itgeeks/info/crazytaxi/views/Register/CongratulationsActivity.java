package itgeeks.info.crazytaxi.views.Register;

import android.content.Intent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.cardview.widget.CardView;
import io.paperdb.Paper;
import itgeeks.info.crazytaxi.General.LocaleHelper;
import itgeeks.info.crazytaxi.R;
import itgeeks.info.crazytaxi.views.HomeActivity;

import java.util.Locale;

import static itgeeks.info.crazytaxi.views.SplashActivity.langContext;
import static itgeeks.info.crazytaxi.views.SplashActivity.langResources;

public class CongratulationsActivity extends AppCompatActivity implements View.OnClickListener {

    TextView navName, tvCongratulations, tvCongratulationsHint,tvCongratulationsStartBooking;
    private LinearLayout btnRegisterCongratulations;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_congratulations);

        initViews();

        //initViews
        btnRegisterCongratulations = findViewById(R.id.btn_register_congratulations);

        updateView(Paper.book().read("language").toString());

        // set Actions
        setActions();
    }

    private void initViews() {
        //initViews
        navName = findViewById(R.id.nav_name);
        tvCongratulations = findViewById(R.id.tv_congratulations);
        tvCongratulationsHint = findViewById(R.id.tv_congratulations_hint);
        tvCongratulationsStartBooking = findViewById(R.id.tv_congratulations_start_booking);

    }

    private void updateView(String lang) {
        langContext = LocaleHelper.setLocale(this, lang);
        langResources = langContext.getResources();

        navName.setText(langResources.getString(R.string.congratulations));
        tvCongratulations.setText(langResources.getString(R.string.congratulations));
        tvCongratulationsHint.setText(langResources.getString(R.string.Your_account_has_been_created));
        tvCongratulationsStartBooking.setText(langResources.getString(R.string.start_booking));

    }

    private void setActions() {
        btnRegisterCongratulations.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_register_congratulations:
                startActivity(new Intent(this, HomeActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                break;
        }
    }
}
