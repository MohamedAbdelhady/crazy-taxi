package itgeeks.info.crazytaxi.views;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Build;
import android.os.Handler;
import android.view.View;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import io.paperdb.Paper;
import itgeeks.info.crazytaxi.General.SharedPrefranceManager;
import itgeeks.info.crazytaxi.R;
import itgeeks.info.crazytaxi.views.Register.RegisterActivity;

import java.util.Locale;

public class SplashActivity extends AppCompatActivity {
    public static Context langContext;
    public static Resources langResources;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        // init language
        initLanguage();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(SplashActivity.this, RegisterActivity.class));
                finish();
            }
        }, 2000);


    }

    public void initLanguage() {

        // init Lang
        Paper.init(this);

        if (!SharedPrefranceManager.getInastance(this).isFirstTimeOpenApp()) {
            Paper.book().write("language", new Locale("en"));
            SharedPrefranceManager.getInastance(this).setFirstTimeOpenApp(true);
        }

        // Change Screen Dirction on API 27

            if (Locale.getDefault().getLanguage().trim().equals("en")) {
                getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            } else {
                getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            }


    }

}
