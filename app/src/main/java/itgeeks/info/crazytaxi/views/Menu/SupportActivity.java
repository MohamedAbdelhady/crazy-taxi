package itgeeks.info.crazytaxi.views.Menu;

import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import io.paperdb.Paper;
import itgeeks.info.crazytaxi.General.LocaleHelper;
import itgeeks.info.crazytaxi.R;

import static itgeeks.info.crazytaxi.views.HomeActivity.langContext;
import static itgeeks.info.crazytaxi.views.HomeActivity.langResources;

public class SupportActivity extends AppCompatActivity {
    private EditText et_search;

    private TextView navName;
    private TextView tvSupportHint1,tvSupportHint2,tvSupportHint3,tvSupportHint4,tvSupportHint5,tvSupportHint6;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_support);

        initViews();

        // Update Language
        updateView(Paper.book().read("language").toString());

    }

    private void initViews() {

        // init views
        navName = findViewById(R.id.nav_name);
        et_search = findViewById(R.id.et_support_search);
        tvSupportHint1 = findViewById(R.id.tv_support_hint1);
        tvSupportHint2 = findViewById(R.id.tv_support_hint2);
        tvSupportHint3 = findViewById(R.id.tv_support_hint3);
        tvSupportHint4 = findViewById(R.id.tv_support_hint4);
        tvSupportHint5 = findViewById(R.id.tv_support_hint5);
        tvSupportHint6 = findViewById(R.id.tv_support_hint6);

        // setActions
        setActions();

        // back
        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void updateView(String lang) {
        langContext = LocaleHelper.setLocale(this, lang);
        langResources = langContext.getResources();

        navName.setText(langResources.getString(R.string.support));

        et_search.setHint(langResources.getString(R.string.search_topics));
        tvSupportHint1.setText(langResources.getString(R.string.support_hint1));
        tvSupportHint2.setText(langResources.getString(R.string.browse_topics));
        tvSupportHint3.setText(langResources.getString(R.string.faqs_hint1));
        tvSupportHint4.setText(langResources.getString(R.string.faqs_hint1));
        tvSupportHint5.setText(langResources.getString(R.string.faqs_hint1));
        tvSupportHint6.setText(langResources.getString(R.string.faqs_hint1));

    }

    private void setActions() {

    }
}