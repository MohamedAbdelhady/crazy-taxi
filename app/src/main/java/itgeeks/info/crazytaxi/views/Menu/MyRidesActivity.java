package itgeeks.info.crazytaxi.views.Menu;

import android.view.View;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import io.paperdb.Paper;
import itgeeks.info.crazytaxi.General.LocaleHelper;
import itgeeks.info.crazytaxi.R;

import static itgeeks.info.crazytaxi.views.HomeActivity.langContext;
import static itgeeks.info.crazytaxi.views.HomeActivity.langResources;

public class MyRidesActivity extends AppCompatActivity {

    private TextView navName;
    private TextView tvMyRidesHint1,tvMyRidesHint2,tvMyRidesHint3,tvMyRidesHint4,tvMyRidesHint5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_rides);

        initViews();

        // Update Language
        updateView(Paper.book().read("language").toString());

    }

    private void initViews() {
        // Nav Name
        navName = findViewById(R.id.nav_name);
        navName.setText(R.string.my_rides);

        tvMyRidesHint1 = findViewById(R.id.tv_my_rides_hint1);
        tvMyRidesHint2 = findViewById(R.id.tv_my_rides_hint2);
        tvMyRidesHint3 = findViewById(R.id.tv_my_rides_hint3);
        tvMyRidesHint4 = findViewById(R.id.tv_my_rides_hint4);
        tvMyRidesHint5 = findViewById(R.id.tv_my_rides_hint5);

        // setActions
        setActions();

        // back
        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void updateView(String lang) {
        langContext = LocaleHelper.setLocale(this, lang);
        langResources = langContext.getResources();

        navName.setText(langResources.getString(R.string.book_your_outstation_ride));
        tvMyRidesHint1.setText(langResources.getString(R.string.my_rides_hint2));
        tvMyRidesHint2.setText(langResources.getString(R.string.my_rides_hint));
        tvMyRidesHint3.setText(langResources.getString(R.string.tower_no));
        tvMyRidesHint4.setText(langResources.getString(R.string.tower_no));
        tvMyRidesHint5.setText(langResources.getString(R.string.finished));

    }


    private void setActions() {

    }
}