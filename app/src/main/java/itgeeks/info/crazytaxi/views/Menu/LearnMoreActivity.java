package itgeeks.info.crazytaxi.views.Menu;

import android.view.View;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import io.paperdb.Paper;
import itgeeks.info.crazytaxi.General.LocaleHelper;
import itgeeks.info.crazytaxi.R;

import static itgeeks.info.crazytaxi.views.HomeActivity.langContext;
import static itgeeks.info.crazytaxi.views.HomeActivity.langResources;

public class LearnMoreActivity extends AppCompatActivity {

    private TextView navName;
    private TextView tvLearnMoreHint1, tvLearnMoreHint2, tvLearnMoreHint3, tvLearnMoreHint4, tvLearnMoreHint5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_learn_more);

        initViews();

        // Update Language
        updateView(Paper.book().read("language").toString());

    }

    private void initViews() {
        // init Views
        navName = findViewById(R.id.nav_name);
        tvLearnMoreHint1 = findViewById(R.id.tv_learn_more_hint1);
        tvLearnMoreHint2 = findViewById(R.id.tv_learn_more_hint2);
        tvLearnMoreHint3 = findViewById(R.id.tv_learn_more_hint3);
        tvLearnMoreHint4 = findViewById(R.id.tv_learn_more_hint4);
        tvLearnMoreHint5 = findViewById(R.id.tv_learn_more_hint5);

        // setActions
        setActions();

        // back
        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void updateView(String lang) {
        langContext = LocaleHelper.setLocale(this, lang);
        langResources = langContext.getResources();

        navName.setText(langResources.getString(R.string.learn_more));

        tvLearnMoreHint1.setText(langResources.getString(R.string.learn_more_hint1));
        tvLearnMoreHint2.setText(langResources.getString(R.string.learn_more_hint2));
        tvLearnMoreHint3.setText(langResources.getString(R.string.learn_more_hint3));
        tvLearnMoreHint4.setText(langResources.getString(R.string.yes));
        tvLearnMoreHint5.setText(langResources.getString(R.string.no));


    }

    private void setActions() {

    }
}