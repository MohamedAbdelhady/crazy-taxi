package itgeeks.info.crazytaxi.views.Register;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Build;
import android.view.View;
import android.widget.*;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import io.paperdb.Paper;
import itgeeks.info.crazytaxi.Adapters.CountryAdapter;
import itgeeks.info.crazytaxi.General.LocaleHelper;
import itgeeks.info.crazytaxi.General.SharedPrefranceManager;
import itgeeks.info.crazytaxi.Models.CountryItem;
import itgeeks.info.crazytaxi.R;
import java.util.ArrayList;
import java.util.Locale;

import static itgeeks.info.crazytaxi.views.SplashActivity.langContext;
import static itgeeks.info.crazytaxi.views.SplashActivity.langResources;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener {


    private ArrayList<CountryItem> mCountryList;
    private CountryAdapter mAdapter;
    private Spinner spinnerCountries;
    private LinearLayout btnRegisterNext;
    private TextView tvCountryCode, navName ,tvRegisterHint,tvRegisterNext;
    private EditText tvRegisterPhone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        initViews();

        updateView(Paper.book().read("language").toString());


        mAdapter = new CountryAdapter(this, mCountryList);
        spinnerCountries.setAdapter(mAdapter);

        spinnerCountries.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                CountryItem clickedItem = (CountryItem) parent.getItemAtPosition(position);
                String countryCode = clickedItem.getmCountryCode();
                tvCountryCode.setText(countryCode);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }




    private void initViews() {


        // initViews
        navName = findViewById(R.id.nav_name);
        spinnerCountries = findViewById(R.id.spinner_countries);
        btnRegisterNext = findViewById(R.id.btn_register_next);
        tvCountryCode = findViewById(R.id.tv_country_code);
        tvRegisterHint = findViewById(R.id.register_hint);
        tvRegisterPhone = findViewById(R.id.tv_register_phone);
        tvRegisterNext = findViewById(R.id.tv_register_next);

        setActions();


        // back
        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void updateView(String lang) {
        langContext = LocaleHelper.setLocale(this, lang);
        langResources = langContext.getResources();

        initList();

        navName.setText(langResources.getString(R.string.enter_mobile_number));
        tvRegisterHint.setText(langResources.getString(R.string.register_hint));
        tvRegisterPhone.setHint(langResources.getString(R.string.your_number));
        tvRegisterNext.setText(langResources.getString(R.string.next));
    }

    private void setActions() {
        btnRegisterNext.setOnClickListener(this);
    }

    private void initList() {
        mCountryList = new ArrayList<>();
        mCountryList.add(new CountryItem(langResources.getString(R.string.egypt), "+20", R.drawable.egypt));
        mCountryList.add(new CountryItem(langResources.getString(R.string.united_states), "+123", R.drawable.us));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_register_next:
                startActivity(new Intent(this, ActiveMobileCodeActivity.class));
                break;
        }
    }
}
