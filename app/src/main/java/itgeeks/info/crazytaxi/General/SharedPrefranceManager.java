package itgeeks.info.crazytaxi.General;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPrefranceManager {
    Context context;

    private static final String SHARED_PREF_FIRST_TIME_OPEN_APP = "crazy_taxi_first_time_open_app";
    private static final String SHARED_PREF_LANG = "crazy_taxi_lang";

    private static SharedPrefranceManager sharedPrefranceManager;

    private SharedPrefranceManager(Context context) {
        this.context = context;
    }

    public synchronized static SharedPrefranceManager getInastance(Context context){
        if (sharedPrefranceManager == null){
            sharedPrefranceManager = new SharedPrefranceManager(context);
        }
        return sharedPrefranceManager;
    }

    // ------------ Lang ------------- //
    public void saveLang(String lang) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREF_LANG, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.putString("appLang", lang);
        editor.apply();
    }

    public String getLang() {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREF_LANG, Context.MODE_PRIVATE);
        return sharedPreferences.getString("appLang", "en");
    }

    // ------------ first time open app ------------- //
    public void setFirstTimeOpenApp(boolean lang) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREF_FIRST_TIME_OPEN_APP, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.putBoolean("openedApp", lang);
        editor.apply();
    }

    public Boolean isFirstTimeOpenApp() {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREF_FIRST_TIME_OPEN_APP, Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean("openedApp",false);
    }

}
