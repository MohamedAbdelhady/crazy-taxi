package itgeeks.info.crazytaxi.Models;

public class CountryItem {
    private String mCountryName;
    private String mCountryCode;
    private int mFlagImage;

    public CountryItem(String countryName,String countryCode ,int flagImage) {
        mCountryName = countryName;
        mCountryCode = countryCode;
        mFlagImage = flagImage;
    }

    public String getmCountryName() {
        return mCountryName;
    }

    public void setmCountryName(String mCountryName) {
        this.mCountryName = mCountryName;
    }

    public String getmCountryCode() {
        return mCountryCode;
    }

    public void setmCountryCode(String mCountryCode) {
        this.mCountryCode = mCountryCode;
    }

    public int getmFlagImage() {
        return mFlagImage;
    }

    public void setmFlagImage(int mFlagImage) {
        this.mFlagImage = mFlagImage;
    }
}